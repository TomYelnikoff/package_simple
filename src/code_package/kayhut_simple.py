def find_prim(x):
    all_prim = []
    if type(x) == int and x >= 2 and x <= 100:
        for number in range(1, x):
            flag = False
            if number > 1:
                for i in range(2, number):
                    if (number % i) == 0:
                        flag = True
                        break
            if not flag:
                all_prim.append(number)
        return all_prim
    else:
        raise Exception("The value is illegal")

def sub_check(x):
    return x - 5
